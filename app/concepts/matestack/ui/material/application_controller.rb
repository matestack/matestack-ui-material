module Matestack
  module Ui
    module Material
      class ApplicationController < ActionController::Base
        protect_from_forgery with: :exception
      end
    end
  end
end
