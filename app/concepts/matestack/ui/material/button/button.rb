module Matestack::Ui::Material::Button
  class Button < Matestack::Ui::Material::Component::Static

    def response
      components {
        button class: "mdc-button #{button_classes}", attributes: button_attributes, disabled: @options[:disabled] do
          div class: "mdc-button__ripple" #always, even if ripple = false
          icon class: 'material-icons mdc-button__icon', text: @options[:icon] if @options[:icon]
          span class: "mdc-button__label", text: @options[:text] if @options[:text]
          yield_components
          icon class: 'material-icons mdc-button__icon', text: @options[:trailing_icon] if @options[:trailing_icon]
        end
      }
    end

    protected

    def button_attributes
      attrs = @options[:attributes] || {}
      attrs.merge!(@options.slice(:style))
      attrs.merge!({"data-mdc-auto-init":"MDCRipple"}) unless @options[:ripple] == false
      attrs
    end

    def button_classes
      classes = []
      classes << @options[:class]
      button_types.each do |type|
        classes << "mdc-button--#{type}" if @options[:type] == type
      end
      classes << "mdc-button--dense" if @options[:dense] == true
      classes.join(" ")
    end

    def button_types
      [:raised, :unelevated, :outlined]
    end

  end

end
