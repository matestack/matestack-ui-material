module Matestack::Ui::Material::Card
  class Card < Matestack::Ui::Material::Component::Static

    def response
      components{
        div id: "components-material-card" do
          div class: "mdc-card" do
            if @options[:media_slot]
              div class: "mdc-card__media" do
                slot @options[:media_slot]
                # div class: "mdc-card__media-content" do
                # end
              end
            end
            div class: "mdc-card__primary-action", tabindex: "0" do
              if @options[:headline] || @options[:headline_slot]
                heading size: 2, class: "mdc-typography mdc-typography--headline6" do
                  plain @options[:headline]
                  slot @options[:headline_slot]
                end
              end
              if @options[:sub_headline] || @options[:sub_headline_slot]
                heading size: 3, class: "mdc-typography mdc-typography--subtitle2" do
                  plain @options[:sub_headline]
                  slot @options[:sub_headline_slot]
                end
              end
              if @options[:body] || @options[:body_slot]
                paragraph
                div class: "mdc-typography mdc-typography--body2" do
                  plain @options[:body]
                  slot @options[:body_slot]
                end
              end
            end
            if @options[:actions_slot]
              div class: "mdc-card__actions" do
                slot @options[:actions_slot]
              end
            end
          end
        end
      }
    end

  end
end