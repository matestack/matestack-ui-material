module Matestack::Ui::Material::Collection::Filter
  class Input < Matestack::Ui::Core::Collection::Filter::Input::Input

    def response
      components {
        div div_config do
          icon class: "material-icons mdc-text-field__icon", text: leading_icon if leading_icon
          input attributes: input_attributes
          icon class: "material-icons mdc-text-field__icon", text: trailing_icon if trailing_icon
          if outlined?
            partial :outlined_variant_label
          else
            partial :filled_variant_label
          end
        end
      }
    end


    protected

    def filled_variant_label
      partial {
        div class: "mdc-line-ripple"
        partial :label
      }
    end

    def outlined_variant_label
      partial {
        div class: "mdc-notched-outline" do
          div class: "mdc-notched-outline__leading"
          div class: "mdc-notched-outline__notch" do
            partial :label
          end
          div class: "mdc-notched-outline__trailing"
        end
      }
    end

    def label
      partial {
        label class: "mdc-floating-label", text: @options[:label], for: input_id if @options[:label]
      }
    end


    def input_id
      @options[:key]
    end

    def div_config
      {
        class: "mdc-text-field #{component_classes}",
        attributes: component_attributes
      }
    end

    def component_attributes
      attrs = @options.slice :style, :id
      attrs['data-mdc-auto-init'] = "MDCTextField"
      attrs
    end

    def component_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-text-field--outlined" if outlined?
      classes << "mdc-text-field--with-leading-icon" if leading_icon
      classes << "mdc-text-field--with-trailing-icon" if trailing_icon
      classes.join(" ")
    end

    def input_attributes
      @tag_attributes.merge!({
        class: "mdc-text-field__input #{@tag_attributes[:class]}",
        "v-model#{'.number' if options[:type] == :number}": input_key,
        type: options[:type],
        id: component_id,
        "@keyup.enter": "submitFilter()",
        ref: "filter.#{attr_key}",
        placeholder: options[:placeholder]
      })
    end

    def leading_icon
      @options[:icon] || @options[:leading_icon]
    end

    def trailing_icon
      @options[:trailing_icon]
    end

    def outlined?
      @options[:material_type].to_s == "outlined"
    end

  end
end
