module Matestack::Ui::Material::Component
  class Static < Matestack::Ui::Core::Component::Static

    view_paths << "#{Matestack::Ui::Material::Engine.root}/app/concepts"

  end
end
