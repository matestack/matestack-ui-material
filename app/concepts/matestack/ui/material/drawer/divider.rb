module Matestack::Ui::Material::Drawer
  class Divider < Matestack::Ui::Material::Component::Static

    def response
      components {
        hr class: "mdc-list-divider #{divider_classes}", attributes: divider_attributes
      }
    end

    protected

    def divider_attributes
      @options.slice :style
    end

    def divider_classes
      classes = []
      classes << @options[:class]
      classes.join(" ")
    end

  end
end