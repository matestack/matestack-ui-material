import MatestackUiCore from 'matestack-ui-core'
import {MDCDrawer} from "@material/drawer";
import {MDCList} from "@material/list";

MatestackUiCore.Vue.component('matestack-ui-material-drawer', {
  mixins: [MatestackUiCore.componentMixin],
  data() {
    return {
      drawerElementId: '',
      drawerElement: {},
      drawerWrapperElement: {},
      drawer: {},
      modal: false
    }
  },
  methods: {
    belongs_to_top_app_bar(top_app_bar) {
      // Check if this drawer belongs to the given top app bar:
      // The drawer's wrapper should be a sibling of the top app bar.
      const self = this
      return self.drawerWrapperElement.parentNode == top_app_bar.topAppBarElement.parentNode
    }
  },
  mounted() {
    const self = this

    self.drawerElementId = self.componentConfig['element_id']
    self.drawerElement = document.getElementById(self.drawerElementId)
    self.drawerWrapperElement = self.drawerElement.closest('.matestack-material-drawer-wrapper')
    self.modal = self.componentConfig['modal']

    // Modal drawers need a different js init than non-modal drawers.
    // See: https://material.io/develop/web/components/drawers/
    //
    if (self.modal) {
      self.drawer = MDCDrawer.attachTo(self.drawerElement)

      MatestackUiCore.matestackEventHub.$on('MDCTopAppBar:nav', (top_app_bar) => {
        if (self.belongs_to_top_app_bar(top_app_bar)) {
          self.drawer.open = ! self.drawer.open
        }
      })

      const listElement = self.drawerElement.querySelector('.mdc-list')
      listElement.addEventListener('click', (event) => {
        self.drawer.open = false
      })

    } else {
      const listElement = document.querySelector('.mdc-list')
      const list = MDCList.attachTo(listElement)
      list.wrapFocus = true
    }
  }
});
