module Matestack::Ui::Material::Drawer
  class Drawer < Matestack::Ui::Material::Component::Dynamic

    def setup
      @component_config[:modal] = modal? ? true : false
      @component_config[:element_id] = drawer_attributes[:id]
    end

    def response
      components {
        div class: 'matestack-material-drawer-wrapper' do
          aside class: "mdc-drawer #{drawer_classes}", attributes: drawer_attributes do
            yield_components
          end
          div class: "mdc-drawer-scrim" if modal?
        end
      }
    end

    protected

    def drawer_attributes
      attrs = @options.slice :style, :id
      attrs[:id] ||= @component_config[:component_key]
      attrs
    end

    def drawer_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-drawer--modal" if modal?
      classes.join(" ")
    end

    def modal?
      @options[:modal] || (@options[:type].to_s == "modal")
    end

  end
end