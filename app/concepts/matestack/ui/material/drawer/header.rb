module Matestack::Ui::Material::Drawer
  class Header < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-drawer__header #{header_classes}", attributes: header_attributes do
          yield_components
        end
      }
    end

    protected

    def header_attributes
      @options.slice :style
    end

    def header_classes
      classes = []
      classes << @options[:class]
      classes.join(" ")
    end

  end
end