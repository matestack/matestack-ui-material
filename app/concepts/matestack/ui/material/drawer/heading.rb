module Matestack::Ui::Material::Drawer
  class Heading < Matestack::Ui::Material::Component::Static

    def response
      components {
        heading size: 6, class: "mdc-list-group__subheader #{heading_classes}", attributes: heading_attributes, text: @options[:text]
      }
    end

    protected

    def heading_attributes
      @options.slice :style
    end

    def heading_classes
      classes = []
      classes << @options[:class]
      classes.join(" ")
    end

  end
end