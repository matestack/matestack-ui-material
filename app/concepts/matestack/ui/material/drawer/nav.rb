module Matestack::Ui::Material::Drawer
  class Nav < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-drawer__content" do
          nav class: "mdc-list #{nav_classes}", attributes: nav_attributes do
            yield_components
          end
        end
      }
    end

    protected

    def nav_attributes
      @options.slice :style
    end

    def nav_classes
      classes = []
      classes << @options[:class]
      classes.join(" ")
    end

  end
end