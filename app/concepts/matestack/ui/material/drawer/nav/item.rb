module Matestack::Ui::Material::Drawer
  class Nav::Item < Matestack::Ui::Material::Component::Static

    def response
      components {
        if transition?
          transition path: @options[:path], params: @options[:params], class: "mdc-list-item  #{item_classes}", attributes: item_attributes do
            partial :item_icon if @options[:icon]
            partial :item_text if @options[:text]
          end
        end
        if link?
          link path: @options[:path], params: @options[:params], class: "mdc-list-item #{item_classes}", attributes: item_attributes do
            partial :item_icon if @options[:icon]
            partial :item_text if @options[:text]
          end
        end
        if action?
          action @options[:action_config], id: "hello", class: "mdc-list-item #{item_classes}", attributes: item_attributes do
            partial :item_icon if @options[:icon]
            partial :item_text if @options[:text]
          end
        end
      }
    end

    protected

    def item_icon
      partial {
        icon class: "material-icons mdc-list-item__graphic" do
          plain @options[:icon]
        end
      }
    end

    def item_text
      partial {
        span class: "mdc-list-item__text" do
          plain @options[:text]
        end
      }
    end

    def item_attributes
      attrs = @options.slice :style
      # element needs to be focusable
      # https://github.com/material-components/material-components-web/issues/762
      attrs.merge!({
        "tabindex": "0"
        # "v-bind:class": "{ 'mdc-list-item--activated': isActive}"
      })
    end

    def item_classes
      classes = []
      classes << @options[:class]
      classes.join(" ")
    end

    def transition?
      @options[:type] == :transition
    end

    def link?
      @options[:type] == :link
    end

    def action?
      @options[:type] == :action
    end

  end
end
