module Matestack::Ui::Material::Form
  class Input < Matestack::Ui::Core::Form::Input::Input

    def setup
      super
      @options[:material_type] = :outlined if textarea? # because filled textareas are not supported in https://material-components.github.io/material-components-web-catalog/#/component/text-field
    end

    def response
      components {
        label label_config, :include do
          icon class: "material-icons mdc-text-field__icon", text: leading_icon if leading_icon
          input type: @options[:type], attributes: input_attributes unless textarea?
          material_form_textarea key: @options[:key], type: :textarea if textarea?
          icon class: "material-icons mdc-text-field__icon", text: trailing_icon if trailing_icon
          if outlined?
            partial :outlined_variant_label
          else
            partial :filled_variant_label unless textarea?
          end
        end
        partial :error_line
      }
    end


    protected

    def filled_variant_label
      partial {
        div class: "mdc-line-ripple"
        partial :label
      }
    end

    def outlined_variant_label
      partial {
        div class: "mdc-notched-outline" do
          div class: "mdc-notched-outline__leading"
          div class: "mdc-notched-outline__notch" do
            partial :label
          end
          div class: "mdc-notched-outline__trailing"
        end
      }
    end

    def label
      partial {
        label class: "mdc-floating-label", text: @options[:label], for: input_id if @options[:label]
      }
    end

    def error_line
      partial {
        div class: "mdc-text-field-helper-line", attributes: {"v-if": error_key} do
          div class: "mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg", "aria-hidden": "true", role: "alert" do
            span attributes: {"v-for": "error in  #{error_key}"} do
              plain "{{ error }}"
            end
          end
        end
      }
    end

    def input_id
      @options[:key]
    end

    def label_config
      @options[:included_config].merge({
        class: "mdc-text-field #{component_classes}",
        attributes: component_attributes
      })
    end

    def component_attributes
      attrs = @options.slice :style, :id
      attrs['data-mdc-auto-init'] = "MDCTextField"
      attrs['v-bind:class'] = "{\"mdc-text-field--invalid\": #{error_key}}"
      attrs
    end

    def component_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-text-field--outlined" if outlined?
      classes << "mdc-text-field--with-leading-icon" if leading_icon
      classes << "mdc-text-field--with-trailing-icon" if trailing_icon
      classes << "mdc-text-field--textarea" if textarea?
      classes.join(" ")
    end

    def input_attributes
      @tag_attributes.merge!({
        class: "mdc-text-field__input #{@tag_attributes[:class]}",
        ref: "input.#{attr_key}",
        type: @options[:type],
        placeholder: @options[:placeholder],
        "v-model#{'.number' if @options[:type] == :number}": input_key,
        "@change": "inputChanged(\"#{attr_key}\")",
        "init-value": init_value
      })
    end

    def leading_icon
      @options[:icon] || @options[:leading_icon]
    end

    def trailing_icon
      @options[:trailing_icon]
    end

    def outlined?
      @options[:material_type].to_s == "outlined"
    end

    def textarea?
      @options[:type].to_s == "textarea"
    end

  end
end
