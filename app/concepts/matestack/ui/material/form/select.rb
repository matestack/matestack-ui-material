module Matestack::Ui::Material::Form
  class Select < Matestack::Ui::Core::Form::Select::Select
    def response
      components {
        case @options[:type].to_s
        when "checkbox"
          material_form_select_checkboxes @options
        when "radio"
          material_form_select_radio_buttons @options
        else
          material_form_select_dropdown @options.merge({
            init_value: init_value, error_key: error_key,
            attributes: { "@update": "updateFormValue", "@reseterrors": "resetErrors", "@register": "initDataKey", "v-bind:data": "data", "v-bind:errors": "errors"},
          })
          # partial :material_select
        end
        # partial :error_section
      }
    end

    protected

    def material_select
      partial {
        div @options[:included_config].merge({class: "mdc-select #{component_classes}", attributes: component_attributes}), :include do
          div class: "mdc-select__anchor", attributes: {role: "button", "aria-haspopup": "listbox"}do
            span class: "mdc-select__ripple"
            div class: "mdc-select__selected-text", attributes: {"@change": "data[\"#{@options[:key]}\"] = $event.value; $forceUpdate()"} do
              plain init_value
            end
            icon class: "mdc-select__dropdown-icon"
            if outlined?
              partial :outlined_variant_label
            else
              partial :filled_variant_label
            end
          end

          div class: "mdc-select__menu mdc-menu mdc-menu-surface", attributes: { role: "listbox" } do
            ul class: "mdc-list" do
              @options[:options].each do |option_label, option_value|
                # selected = ((option_value.to_s == init_value.to_s))
                li class: "mdc-list-item #{'mdc-list-item--selected' if false}", text: option_label, attributes: {'data-value': option_value || option_label, "v-bind:class": "{ \"mdc-list-item--selected\": data[\"#{@options[:key]}\"] == \"#{option_value}\"}"}
              end
            end
          end
        end
      }
    end

    def label
      partial {
        span class: "mdc-floating-label", text: @options[:label] if @options[:label]
      }
    end

    def outlined_variant_label
      partial {
        span class: "mdc-notched-outline" do
          span class: "mdc-notched-outline__leading"
          span class: "mdc-notched-outline__notch" do
            partial :label
          end
          span class: "mdc-notched-outline__trailing"
        end
      }
    end

    def filled_variant_label
      partial {
        partial :label
        span class: "mdc-line-ripple"
      }
    end

    def error_section
      partial {
        span class: "errors", attributes: {'v-if': error_key} do
          span class: "error", attributes: {'v-for': "error in #{error_key}"} do
            plain "{{ error }}"
          end
        end
      }
    end

    def component_attributes
      attrs = @options.slice :style, :id
      attrs['data-mdc-auto-init'] = "MDCSelect"
      attrs
    end

    def component_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-select--outlined" if outlined?
      classes.join(" ")
    end

    def outlined?
      @options[:material_type].to_s == "outlined"
    end

  end
end
