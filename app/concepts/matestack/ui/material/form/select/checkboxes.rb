module Matestack::Ui::Material::Form
  class Select::Checkboxes < Select::Options

    def response
      components {
        div attributes: wrapper_div_attributes do
          partial :common_label if @options[:label]
          @options[:options].each do |option_label, option_value|
            material_form_select_single_checkbox @options.merge({label: option_label, value: option_value})
          end
        end
      }
    end

    def wrapper_div_attributes
      {
        'init-value': init_value,
        ref: "select.multiple.#{@options[:key]}"
      }
    end

  end
end
