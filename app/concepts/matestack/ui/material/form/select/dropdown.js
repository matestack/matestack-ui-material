import MatestackUiCore from 'matestack-ui-core'
import {MDCSelect} from '@material/select';
import {MDCSelectHelperText} from '@material/select/helper-text'

MatestackUiCore.Vue.component('matestack-ui-material-form-select-dropdown', {
  mixins: [MatestackUiCore.componentMixin],
  data() {
    return {
      select: null,
      helperText: null
    }
  },
  props: ['data', 'errors'],
  watch: {
    data: function (newVal, oldVal) {
      if(newVal[this.componentConfig["key"]] == null){
        this.init()
      }
    }
  },
  methods: {
    isActive(option){
      return option == this.data[this.componentConfig["key"]]
    },
    init(){
      const self = this

      setTimeout(function () {
        self.$emit("register", self.componentConfig["key"], self.componentConfig["init_value"])
      }, 500);

      self.select.foundation_.setValue(self.componentConfig["init_value"])
      self.$forceUpdate()
      self.$parent.$forceUpdate()
    }
  },
  mounted() {
    const self = this

    self.select = new MDCSelect(self.$refs["dropdown"]);
    self.helperText = new MDCSelectHelperText(self.$refs["helpertext"]);

    self.init()

    self.select.listen('MDCSelect:change', (data) => {
      if(self.select.value === ""){
        self.$emit("update", self.componentConfig["key"], null)
        self.$emit("reseterrors", self.componentConfig["key"])
        self.$forceUpdate()
        self.$parent.$forceUpdate()
      } else {
        self.$emit("register", self.componentConfig["key"], self.select.value)
        self.$emit("reseterrors", self.componentConfig["key"])
        self.$parent.$forceUpdate()
      }
    })
  }
});
