module Matestack::Ui::Material::Form
  class Select::Dropdown < Matestack::Ui::Core::Component::Dynamic

    def response
      components {
        div do
          partial :material_select
          partial :error_section
        end
      }
    end

    def material_select
      partial {
        div class: "mdc-select #{component_classes}", attributes: component_attributes do
          div class: "mdc-select__anchor", attributes: {role: "button", "aria-haspopup": "listbox"}do
            span class: "mdc-select__ripple"
            div class: "mdc-select__selected-text"
            icon class: "mdc-select__dropdown-icon"
            if outlined?
              partial :outlined_variant_label
            else
              partial :filled_variant_label
            end
          end

          div class: "mdc-select__menu mdc-menu mdc-menu-surface", attributes: { role: "listbox" } do
            ul class: "mdc-list" do
              @options[:options].each do |option_label, option_value|
                li class: "mdc-list-item", text: option_label, attributes: {'data-value': data_value(option_value, option_label), "v-bind:class": "{ \"mdc-list-item--selected\": isActive(\"#{option_value}\") || isActive(\"#{option_label}\") }"}
              end
            end
          end
        end
      }
    end

    def data_value (option_value, option_label)
      value = option_value || option_label
    end

    def label
      partial {
        span class: "mdc-floating-label", text: @options[:label] if @options[:label]
      }
    end

    def outlined_variant_label
      partial {
        span class: "mdc-notched-outline" do
          span class: "mdc-notched-outline__leading"
          span class: "mdc-notched-outline__notch" do
            partial :label
          end
          span class: "mdc-notched-outline__trailing"
        end
      }
    end

    def filled_variant_label
      partial {
        partial :label
        span class: "mdc-line-ripple"
      }
    end

    def error_section
      partial {
        paragraph class: "mdc-select-helper-text mdc-select-helper-text--persistent mdc-select-helper-text--validation-msg", attributes: {'v-if': @options[:error_key], ref: "helpertext", style: error_styles} do
          span class: "error", attributes: {'v-for': "error in #{@options[:error_key]}"} do
            plain "{{ error }}"
          end
        end
      }
    end

    def error_styles
      "padding-right: 16px; padding-left: 16px; color: var(--mdc-theme-error);"
    end

    def component_attributes
      attrs = @options.slice :style, :id
      attrs['data-mdc-auto-init'] = "MDCSelect"
      attrs['ref'] = "dropdown"
      attrs['v-bind:class'] = "{ 'mdc-select--invalid': #{@options[:error_key]} }"
      attrs
    end

    def component_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-select--outlined" if outlined?
      classes.join(" ")
    end

    def outlined?
      @options[:material_type].to_s == "outlined"
    end

  end
end
