module Matestack::Ui::Material::Form
  class Select::Options < Select

    def common_label
      partial {
        label text: @options[:label]
      }
    end

  end
end
