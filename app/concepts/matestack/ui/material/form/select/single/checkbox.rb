module Matestack::Ui::Material::Form
  class Select::Single::Checkbox < Select::Single::Option
    def response
      me = self
      components {
        div class: "mdc-form-field", attributes: {'data-mdc-auto-init': "MDCFormField"} do
          div class: "mdc-checkbox", attributes: {'data-mdc-auto-init': "MDCCheckbox"} do
            input class: "mdc-checkbox__native-control", type: :checkbox, attributes: me.input_attributes
            div class: "mdc-checkbox__background" do
              plain "<svg class=\"mdc-checkbox__checkmark\" viewBox=\"0 0 24 24\"><path class=\"mdc-checkbox__checkmark-path\" fill=\"none\" d=\"M1.73,12.91 8.1,19.28 22.79,4.59\"/></svg>".html_safe
            end
            div class: "mdc-checkbox__mixedmark"
            div class: "mdc-checkbox__ripple"
          end
          label for: me.input_id, text: me.label
        end
      }
    end

    def input_attributes
      super.merge({type: :checkbox})
    end

    def initially_selected
      @initially_selected ||= (value.to_s.in? initially_selected_values) or (label.to_s.in? initially_selected_values)
    end

    def initially_selected_values
      @initially_selected_values ||= init_value.map(&:to_s)
    end

  end
end
