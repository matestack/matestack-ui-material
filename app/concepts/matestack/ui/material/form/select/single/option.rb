module Matestack::Ui::Material::Form
  class Select::Single::Option < Select

    def input_id
      @input_id ||= "#{@options[:key]}_#{value.to_s.underscore}_input"
    end

    def value
      @value ||= @options[:value] || @options[:label]
    end

    def label
      @options[:label]
    end

    def input_attributes
      {
        id: input_id,
        name: @options[:key],
        value: value,
        model_binding => input_key,
        '@change' => "inputChanged(\"#{attr_key}\")",
        value_type: options_type,
        checked: initially_selected
      }
    end

  end
end