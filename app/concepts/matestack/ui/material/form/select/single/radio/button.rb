module Matestack::Ui::Material::Form
  class Select::Single::Radio::Button < Select::Single::Option
    def response
      me = self
      components {
        div class: "mdc-form-field", attributes: {'data-mdc-auto-init': "MDCFormField"} do
          div class: "mdc-radio", attributes: {'data-mdc-auto-init': "MDCRadio"} do
            input class: "mdc-radio__native-control", type: :radio, attributes: me.input_attributes
            div class: "mdc-radio__background" do
              div class: "mdc-radio__outer-circle"
              div class: "mdc-radio__inner-circle"
            end
            div class: "mdc-radio__ripple"
          end
          label for: me.input_id, text: me.label
        end
      }
    end

    def input_attributes
      super.merge({type: :radio})
    end

    def initially_selected
      @initially_selected ||= (value.to_s == init_value.to_s) or (label.to_s == init_value.to_s)
    end

  end
end
