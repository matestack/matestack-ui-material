module Matestack::Ui::Material::Grid
  class Cell < Matestack::Ui::Material::Component::Static

    def prepare
      # https://basemates.slack.com/archives/GPV5T7T4N/p1578140755015100?thread_ts=1578139648.007400&cid=GPV5T7T4N
      @options[:span] ||= @options[:cols] || @options[:columns] || @options[:width] || @options[:size]
    end

    def response
      components {
        div class: "mdc-layout-grid__cell #{cell_classes}", attributes: cell_attributes do
          yield_components
        end
      }
    end

    protected

    def cell_attributes
      attrs = @options.slice :style
    end

    def cell_classes
      classes = []
      %i(desktop tablet phone).each do |device_type|
        classes << "mdc-layout-grid__cell--span-#{@options[device_type]}-#{device_type}" if @options[device_type]
      end
      %i(span order align).each do |configuration_key|
        classes << "mdc-layout-grid__cell--#{configuration_key}-#{@options[configuration_key]}" if @options[configuration_key]
      end
      classes << @options[:class] if @options[:class]
      classes.join(" ")
    end

  end
end