module Matestack::Ui::Material::Grid
  class Grid < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-layout-grid #{grid_classes}", attributes: grid_attributes do
          div class: "mdc-layout-grid__inner", attributes: inner_attributes do
            yield_components
          end
        end
      }
    end

    protected

    def grid_attributes
      attrs = @options.slice :style
    end

    def inner_attributes
      attrs = {}
      attrs[:style] = @options[:inner_style] if @options[:inner_style]
      attrs
    end

    def grid_classes
      classes = []
      classes << "mdc-layout-grid--fixed-column-width" if @options[:fixed_column_width]
      classes << "mdc-layout-grid--align-#{@options[:align]}" if @options[:align]
      classes << @options[:class] if @options[:class]
      classes.join(" ")
    end

  end
end