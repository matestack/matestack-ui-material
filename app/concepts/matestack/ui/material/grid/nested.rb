module Matestack::Ui::Material::Grid
  class Nested < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-layout-grid__inner #{nested_classes}", attributes: nested_attributes do
          yield_components
        end
      }
    end

    protected

    def nested_attributes
      @options.slice :style
    end

    def nested_classes
      @options[:class]
    end

  end
end