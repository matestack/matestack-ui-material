module Matestack::Ui::Material::Heading
  class Heading < Matestack::Ui::Material::Component::Static

    def response
      components {
        heading heading_attributes do
          yield_components
        end
      }
    end

    protected

    def heading_attributes
      @tag_attributes.merge!({
        size: @options[:size] ||= 1,
        class: "mdc-typography--headline#{@options[:size] ||= 1} #{@tag_attributes[:class]}",
        text: @options[:text]
      })
    end
  end

end
