module Matestack::Ui::Material::Smart::Table
  class Table < Matestack::Ui::Material::Component::Static
    include Matestack::Ui::Core::Collection::Helper

    def prepare
      my_collection_id = @options[:id] ||= "smart-table"

      model = @options[:base_query].model
      model_name = model.model_name
      controller_path = @options[:context][:request][:controller]

      # Find out the headers via i18n if only the attribute keys are given.
      # [:id, :name]  =>  {id: "Client-ID", name: "Name"}
      #
      if @options[:include].kind_of? Array
        @options[:include] = @options[:include].map { |attribute_key|
          label = model.human_attribute_name(attribute_key.to_s.gsub(".", "_"))
          [attribute_key, label]
        }.to_h
      end

      # If only an array of attribute keys is given as filter,
      # find the best auto config for them based on attribue names
      # and attribute types.
      #
      if @options[:filter].kind_of? Array
        @options[:filter] = @options[:filter].map { |attribute_key|
          attribute_label = model.human_attribute_name(attribute_key.to_s.gsub(".", "_"))
          [attribute_key, {
            label: "Filtern nach #{attribute_label}",
            operator: (model.columns_hash[attribute_key.to_s].try(:type) == :integer ? :equals : :like)
          }]
        }
      end

      # If the row_action is just `:show` or `:edit`, deduce the best configuration
      # automatically.
      #
      if @options[:row_action] == :show and controller_path
        @options[:row_action] = {
          type: :transition,
          params: {id: :id},
          path: "#{controller_path.gsub("/", "_").singularize}_path".to_sym
        }
      end
      if @options[:row_action] == :edit and controller_path
        @options[:row_action] = {
          type: :transition,
          params: {id: :id},
          path: "edit_#{controller_path.gsub("/", "_").singularize}_path".to_sym
        }
      end

      # If an edit_action is given, but it's just `true`, deduce the best
      # configuration automatically.
      #
      if @options[:edit_action] == true and controller_path
        @options[:edit_action] = {
          type: :transition,
          params: {id: :id},
          path: "edit_#{controller_path.gsub("/", "_").singularize}_path".to_sym
        }
      end

      # If a delete_action is given, but it's just `true`, deduce the best
      # configuration automatically.
      #
      default_delete_action = {
        method: :delete,
        path: "#{controller_path.gsub("/", "_").singularize}_path".to_sym,
        success: {emit: "#{my_collection_id}-update"}
      }
      if @options[:delete_action] == true and controller_path
        @options[:delete_action] = default_delete_action
      end

      # If a delete_action is given, but it's missing a path, deduce the best
      # auto configuration and mere it with the given options.
      #
      # This is useful if the user jsut wants to pass a `disabled` condition.
      #
      if @options[:delete_action].kind_of? Hash and @options[:delete_action][:path].blank?
        @options[:delete_action] = default_delete_action.merge @options[:delete_action]
      end

      current_filter = get_collection_filter(my_collection_id)
      # current_order = get_collection_order(my_collection_id)

      my_base_query = @options[:base_query]

      my_filtered_query = my_base_query

      if @options[:order].present?
        my_filtered_query = my_base_query.order(@options[:order])
      end

      if @options[:filter].present?
        @options[:filter].each do |key, filter_options|
          value = current_filter[key.to_sym]
          if value.present? && key.to_s.include?(".")
            associated_name = key.to_s.split(".").first
            my_filtered_query = my_filtered_query.joins(associated_name.to_sym)
            table_name = model.reflections[associated_name].table_name
            key = key.to_s.gsub(associated_name, table_name)
          end
          if value.present?
            key = key.to_sym
            if model.new.respond_to?(key) && (not key.to_s.in? model.attribute_names)
              ids = my_filtered_query.to_a.select { |record|
                if filter_options[:operator] == :like
                  record.send(key).to_s.include? value
                else
                  record.send(key).to_s == value
                end
              }.map(&:id)
              my_filtered_query = my_filtered_query.where(id: ids)
            else
              case filter_options[:operator]
              when :equals
                my_filtered_query = my_filtered_query.where("#{key}": value)
              when :like
                my_filtered_query = my_filtered_query.where("lower(#{key}) LIKE ?", "%#{value.downcase}%")
              else
                my_filtered_query = my_filtered_query
              end
            end
          end
        end
      end

      if @options[:paginate].present?
        @my_collection = set_collection({
          id: my_collection_id,
          data: my_filtered_query,
          init_limit: @options[:paginate], #set a limit
          filtered_count: my_filtered_query.count, #tell the component how to count the filtered result
          base_count: my_base_query.count #tell the component how to count the total amount
        })
      else
        @my_collection = set_collection({
          id: my_collection_id,
          data: my_filtered_query
        })
      end
    end

    def response
      components {
        div id: "components-material-smart-table" do
          if @options[:filter].present?
            partial :filter
            br
            br
          end
          # the content has to be wrapped in an `async` component
          # the event has to be "your_custom_collection_id" + "-update"
          # async not working behind yield_components at the moment
          # --> async with the appropriate event listener have to be placed before yield_components
          # async rerender_on: "my-first-collection-update" do
          partial :content
          # end
        end

      }
    end

    def filter
      partial {
        div class: "filter" do
          collection_filter @my_collection.config do
            @options[:filter].each do |key, filter_options|
              material_collection_filter_input material_type: :outlined, key: key, type: :text, label: filter_options[:label]
            end
            collection_filter_submit do
              material_button text: "filter", icon: "filter_list"
            end
            collection_filter_reset do
              material_button text: "reset", icon: "refresh"
            end
          end
        end
      }
    end

    def content
      partial {
        collection_content @my_collection.config do
          div class: "mdc-data-table" do
            table class: "mdc-data-table__table", "aria-label": "Dessert calories" do
              thead do
                tr class: "mdc-data-table__header-row" do
                  @options[:include].each do |key, value|
                    th class: "mdc-data-table__header-cell", role: "columnheader", scope: "col" do
                      plain value
                    end
                  end
                  if @options[:edit_action].present?
                    th class: "mdc-data-table__header-cell", role: "columnheader", scope: "col" do
                      plain "Bearbeiten"
                    end
                  end
                  if @options[:delete_action].present?
                    th class: "mdc-data-table__header-cell", role: "columnheader", scope: "col" do
                      plain "Löschen"
                    end
                  end
                end
              end
              tbody class: "mdc-data-table__content" do
                @my_collection.paginated_data.each do |instance|
                  tr class: "mdc-data-table__row content-row" do
                    @options[:include].each do |_key, value|
                      td class: "mdc-data-table__cell" do
                        label = instance.instance_eval(_key.to_s)
                        #label = I18n.translate(label, default: label.to_s) if label.kind_of? Symbol
                        if @options[:row_action].present?
                          if @options[:row_action][:type] == :link
                            link path: @options[:row_action][:path], params: get_instance_params_hash(instance) do
                              plain label
                            end
                          end
                          if @options[:row_action][:type] == :transition
                            transition path: @options[:row_action][:path], params: get_instance_params_hash(instance) do
                              plain label
                            end
                          end
                        else
                          plain label
                        end
                      end
                    end
                    if @options[:edit_action].present?
                      td class: "mdc-data-table__cell" do
                        if @options[:edit_action][:type] == :link
                          link path: @options[:edit_action][:path], params: get_instance_params_hash(instance) do
                            material_button icon: 'edit'
                          end
                        end
                        if @options[:edit_action][:type] == :transition
                          transition path: @options[:edit_action][:path], params: get_instance_params_hash(instance) do
                            material_button icon: 'edit'
                          end
                        end
                      end
                    end
                    if @options[:delete_action].present?
                      if @options[:delete_action][:disabled].respond_to?(:call) && @options[:delete_action][:disabled].call(instance)
                        td class: "mdc-data-table__cell", attributes: {title: @options[:delete_action][:disabled_message]} do
                          material_button icon: 'delete', disabled: true
                        end
                      else
                        td class: "mdc-data-table__cell" do
                          delete_action_config = {
                            params: get_instance_params_hash(instance),
                            method: :delete
                          }.merge(@options[:delete_action])
                          action delete_action_config do
                            material_button icon: 'delete'
                          end
                        end
                      end
                    end
                  end
                end
                if @options[:paginate].present?
                  tr class: "mdc-data-table__row footer-row" do
                    @options[:include].each_with_index do |(key, value), index|
                      td class: "mdc-data-table__cell paginator-col", attributes: { align: "right" } do
                        partial :paginator if index == @options[:include].count-1
                      end
                    end
                  end
                end
              end
            end
          end
        end
      }
    end

    def paginator
      partial {
        div class: "paginator-icons" do
          collection_content_previous do
            icon class: "material-icons", text: "keyboard_arrow_left"
          end

          # @my_collection.pages.each do |page|
          #   collection_content_page_link page: page do
          #     button text: page
          #   end
          # end

          collection_content_next do
            icon class: "material-icons", text: "keyboard_arrow_right"
          end
        end
        div class: "paginator-text" do
          plain "#{@my_collection.from}"
          plain "- #{@my_collection.to}"
          plain "von #{@my_collection.filtered_count}"
          unless @my_collection.base_count - @my_collection.filtered_count == 0
            plain "(#{@my_collection.base_count - @my_collection.filtered_count} gefiltert)"
          end
        end
      }
    end

    def get_instance_params_hash instance
      result = {}
      @options[:row_action][:params].each do |key, value|
        result[key] = instance.send(value)
      end
      return result
    end
  end
end
