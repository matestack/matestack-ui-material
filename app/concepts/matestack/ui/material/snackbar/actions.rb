module Matestack::Ui::Material::Snackbar
  class Actions < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-snackbar__actions" do
          yield_components
        end
      }
    end

  end
end
