module Matestack::Ui::Material::Snackbar
  class Dismiss < Matestack::Ui::Material::Component::Static

    def response
      components {
        onclick emit: 'dismiss' do
          button class: "mdc-icon-button mdc-snackbar__dismiss material-icons", text: 'close'
        end
      }
    end

  end
end
