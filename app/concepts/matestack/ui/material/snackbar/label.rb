module Matestack::Ui::Material::Snackbar
  class Label < Matestack::Ui::Material::Component::Static

    def response
      components {
        div class: "mdc-snackbar__label" do
          plain @options[:text]
          yield_components
        end
      }
    end

  end
end
