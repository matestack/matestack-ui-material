module Matestack::Ui::Material::Snackbar
  class Snackbar < Matestack::Ui::Material::Component::Static

    def response
      me = self
      components {
        async async_config do
          div class: "mdc-snackbar mdc-snackbar--open", attributes: {'data-mdc-auto-init': "MDCSnackbar"} do
            div class: "mdc-snackbar__surface" do
              material_snackbar_label text: @options[:text] if @options[:text]
              yield_components
              me.add_class_to_components "mdc-snackbar__action", 'material_button', @hash
              partial :dismiss_button_action if @options[:text] # && (not self.get_children.keys.join.include? "snackbar_dismiss")
            end
          end
        end
      }
    end

    def async_config
      {
        show_on: @options[:show_on],
        hide_on: [@options[:hide_on], "dismiss"].join(","),
        hide_after: @options[:hide_after]
      }
    end

    def dismiss_button_action
      partial {
        material_snackbar_actions do
          material_snackbar_dismiss
        end
      }
    end

    def add_class_to_components(css_class, component, hash)
      self.transform_hash(hash) do |key, value|
        if value && value["component_name"] == component
          value["config"][:class] = "#{value["config"][:class]} #{css_class}" unless value["config"][:class].to_s.include? css_class
        end
        value
      end
    end

    def transform_hash(hash, &block)
      hash.each do |key, value|
        new_value = block.call(key, value)
        transform_hash(new_value, &block) if new_value.kind_of? Hash
        hash[key] = new_value
      end
    end

  end
end
