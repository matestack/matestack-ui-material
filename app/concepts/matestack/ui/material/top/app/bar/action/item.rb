module Matestack::Ui::Material::Top::App::Bar
  class Action::Item < Matestack::Ui::Material::Component::Static

    def response
      components {
        partial :icon_item if @options[:icon]
        partial :text_item if @options[:text]
      }
    end

    protected

    def button_attributes
      attrs = @options[:attributes] || {}
      attrs.merge! @options.slice :style
      attrs[:style] = (attrs[:style].to_s.split(";") + ["color: white"]).join("; ") # hack the text color to be white because material only supports icon-based action items and we use a regular material text button here for the moment. FIXME
      attrs
    end

    def button_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-top-app-bar__action-item--unbounded"
      classes << "mdc-icon-button material-icons" if @options[:icon]
      classes.join(" ")
    end

    def icon_item
      partial {
        button class: button_classes, attributes: button_attributes do
          plain @options[:icon]
        end
      }
    end

    def text_item
      partial {
        material_button class: button_classes, attributes: button_attributes do
          plain @options[:text]
        end
      }
    end

  end
end