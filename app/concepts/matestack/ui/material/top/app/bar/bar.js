import MatestackUiCore from 'matestack-ui-core'
import {MDCTopAppBar} from '@material/top-app-bar';

MatestackUiCore.Vue.component('matestack-ui-material-top-app-bar', {
  mixins: [MatestackUiCore.componentMixin],
  data() {
    return {
      topAppBarElementId: '',
      topAppBarElement: {},
      topAppBar: {}
    }
  },
  mounted() {
    const self = this

    self.topAppBarElementId = self.componentConfig['element_id']
    self.topAppBarElement = document.getElementById(self.topAppBarElementId)
    self.topAppBar = MDCTopAppBar.attachTo(self.topAppBarElement)

    self.topAppBar.listen('MDCTopAppBar:nav', () => {
      MatestackUiCore.matestackEventHub.$emit("MDCTopAppBar:nav", self)
    })
  }
});
