module Matestack::Ui::Material::Top::App::Bar
  class Bar < Matestack::Ui::Material::Component::Dynamic

    def setup
      @component_config[:element_id] = bar_attributes[:id]
    end

    def response
      components {
        header class: "mdc-top-app-bar #{bar_classes}", attributes: bar_attributes do
          div class: 'mdc-top-app-bar__row' do
            partial :bar_nav_and_title if @options[:text]
            yield_components
          end
        end
      }
    end

    protected

    def bar_attributes
      attrs = @options.slice :style, :id
      attrs[:id] ||= @component_config[:component_key]
      attrs
    end

    def bar_classes
      classes = []
      classes << @options[:class]
      classes << "mdc-top-app-bar--fixed" if fixed?
      classes.join(" ")
    end

    def fixed?
      @options[:fixed] || (@options[:type].to_s == "fixed")
    end

    def bar_nav_and_title
      partial {
        material_top_app_bar_left do
          material_top_app_bar_navigation_button icon: 'menu'
          material_top_app_bar_title text: @options[:text]
        end
      }
    end

  end
end