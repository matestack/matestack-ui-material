module Matestack::Ui::Material::Top::App::Bar
  class Navigation::Button < Matestack::Ui::Material::Component::Static

    def response
      components {
        button class: "material-icons mdc-top-app-bar__navigation-icon mdc-icon-button #{button_classes}", attributes: button_attributes do
          plain @options[:icon]
        end
      }
    end

    protected

    def button_attributes
      @options.slice :style
    end

    def button_classes
      @options[:class]
    end

  end
end