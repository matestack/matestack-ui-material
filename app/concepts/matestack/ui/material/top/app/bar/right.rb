module Matestack::Ui::Material::Top::App::Bar
  class Right < Matestack::Ui::Material::Component::Static

    def response
      components {
        section class: "mdc-top-app-bar__section mdc-top-app-bar__section--align-end #{section_classes}", attributes: section_attributes do
          yield_components
        end
      }
    end

    protected

    def section_attributes
      @options.slice :style
    end

    def section_classes
      @options[:class]
    end

  end
end