module Matestack::Ui::Material::Top::App::Bar
  class Title < Matestack::Ui::Material::Component::Static

    def response
      components {
        span class: "mdc-top-app-bar__title #{title_classes}", attributes: title_attributes do
          yield_components || plain(@options[:text])
        end
      }
    end

    protected

    def title_attributes
      @options.slice :style
    end

    def title_classes
      @options[:class]
    end

  end
end