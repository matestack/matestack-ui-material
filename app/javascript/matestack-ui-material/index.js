import MatestackUiCore from 'matestack-ui-core'

import * as mdc from 'material-components-web'
import './mdc_auto_init'

import top_app_bar from '../../concepts/matestack/ui/material/top/app/bar/bar'
import drawer from '../../concepts/matestack/ui/material/drawer/drawer'
// import select from '../../concepts/matestack/ui/material/form/select'
import select__dropdown from '../../concepts/matestack/ui/material/form/select/dropdown'

import 'material-components-web/dist/material-components-web.min.css'
import styles from './styles/index.scss'

const MatestackUiMaterial = {
  mdc
}

window.MatestackUiMaterial = MatestackUiMaterial

export default MatestackUiMaterial
