import MatestackUiCore from 'matestack-ui-core'
import * as mdc from 'material-components-web'


// HANDLING COMPONENT INIT
// https://material.io/develop/web/components/auto-init/

// init mdc-components after inital page load
document.addEventListener("DOMContentLoaded", function(event) {
  mdc.autoInit();
});


// re-init mdc components after page transition
MatestackUiCore.matestackEventHub.$on("page_loaded", function(){
  // need timeout, otherwise auto-init has no effect on first page transition
  // tbd
  setTimeout(function () {
    mdc.autoInit();
  }, 1);

})

// quick fix --> tbd
// re-init mdc components after collection update
MatestackUiCore.matestackEventHub.$on("collection-update", function(){
  // need timeout, otherwise auto-init has no effect on first page transition
  // tbd
  setTimeout(function () {
    mdc.autoInit();
  }, 500);

})
