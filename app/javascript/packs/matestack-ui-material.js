// Add polyfills for the asset pipeline
// https://github.com/matestack/matestack-ui-core/issues/238#issuecomment-583905838
//
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import 'element-closest-polyfill'

import '../matestack-ui-material/index'
export default MatestackUiMaterial
