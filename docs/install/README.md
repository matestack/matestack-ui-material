# Install

First of all: Install matestack-ui-core as described here:  https://www.matestack.org/docs/install

If your're using the classic Rails assets pipeline, this guide shows you how to
add matestack.ui-material to your Rails app.

## Gemfile

Add 'matestack-ui-material' to your Gemfile

(Make sure to get your access token at matestack.io)

```ruby
gem 'matestack-ui-material', '~> 0.7.0', :git => 'https://x-access-token:XXXXXX@gitlab.com/matestack/matestack-ui-material.git'
```

and run

```shell
$ bundle install
```

## Javascript

Require 'matestack-ui-material' in your `assets/javascript/application.js`

```javascript
//= require matestack-ui-material
```

## Stylesheet

Require 'matestack-ui-material' in your `assets/stylesheets/application.css`

```css
/*
 *= require matestack-ui-material.min
 */
```
