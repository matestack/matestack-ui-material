module Matestack
  module Ui
    module Material
      class Engine < ::Rails::Engine
        isolate_namespace Matestack::Ui::Material
      end
    end
  end
end
