$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "matestack/ui/material/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "matestack-ui-material"
  spec.version     = Matestack::Ui::Material::VERSION
  spec.authors     = ["Jonas Jabari","Pascal Wengerter"]
  spec.email       = ["info@matestack.org"]
  spec.homepage    = "https://matestack.org"
  spec.summary     = "MaterializeCSS components for matestack UI"
  spec.description = "MaterializeCSS components for matestack UI"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.2"
  spec.add_dependency "matestack-ui-core", "~> 0.7.0"
end
