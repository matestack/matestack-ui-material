class Demo::ApiController < ApplicationController

  def form_with_error
    errors = {}
    [:text_value_key, :dropdown_value_key, :dropdown_2_value_key].each do |key|
      if some_form_params[key].blank?
        errors[key] = ["can't be blank"]
      end
    end
    if errors.any?
      render json: { errors:  errors }, status: 400
    else
      render json: {}, status: 200
    end
  end

  protected

  def some_form_params
    params.require(:my_object).permit(:dropdown_value_key, :dropdown_2_value_key, :text_value_key)
  end

end
