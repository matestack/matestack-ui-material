class Demo::PagesController < ApplicationController

  include Matestack::Ui::Core::ApplicationHelper

  def auto_resolve
    component_name = params[:component]
    responder_for("Pages::Demo::#{component_name.camelcase}".constantize)
  end

end
