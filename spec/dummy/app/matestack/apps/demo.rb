class Apps::Demo < Matestack::Ui::App

  def response
    components {
      material_top_app_bar fixed: true do
        material_top_app_bar_left do
          material_top_app_bar_navigation_button icon: 'menu'
          material_top_app_bar_title text: "Matestack-Ui-Material Demo App"
        end
      end
      material_drawer modal: true do
        material_drawer_nav do
          material_drawer_heading text: "Material Components"
          demo_component_keys.each do |component_key|
            material_drawer_nav_item type: :transition, path: :demo_path, params: { component: component_key }, text:component_key
          end
        end
      end
      main do
        page_content
      end
    }
  end

  def demo_component_keys
    %w(heading grid button top_app_bar drawer form snackbar)
  end

end
