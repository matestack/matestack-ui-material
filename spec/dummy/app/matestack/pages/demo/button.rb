class Pages::Demo::Button < Matestack::Ui::Page

  def response
    components {
      heading size: 2, class: "mdc-typography--headline2", text: "Button"

      heading size: 3, class: "mdc-typography--headline3", text: "Button Variants"

      heading size: 4, class: "mdc-typography--headline4", text: "Text Buttons (default)"
      demo do
        material_button text: "Text button"
        material_button dense: true, text: "Dense text button"
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Raised Button"
      demo do
        material_button type: :raised, text: "Raised button"
        material_button dense: true, type: :raised, text: "Dense raised button"
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Unelevated Button"
      demo do
        material_button type: :unelevated, text: "Unelevated button"
        material_button dense: true, type: :unelevated, text: "Dense unelevated button"
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Outlined Button"
      demo do
        material_button type: :outlined, text: "Outlined button"
        material_button dense: true, type: :outlined, text: "Dense outlined button"
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Buttons with Icons"
      demo do
        material_button type: :raised, text: "Button with icon", icon: :local_pizza
        material_button type: :raised, text: "Button with trailing icon", trailing_icon: :local_pizza
      end
      paragraph
      plain "Available material icons: "
      link path: "https://material.io/resources/icons", target: '_blank', text: "https://material.io/resources/icons"

      heading size: 3, class: "mdc-typography--headline3", text: "Buttons with Blocks"
      demo do
        material_button type: :raised do
          icon class: "material-icons mdc-button__icon", text: "menu_book"
          span class: "mdc-button__label", text: "Button with icon and label in a block"
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Disabled Buttons"
      demo do
        material_button disabled: true, type: :unelevated, text: "Disabled unelevated button"
      end
    }
  end

end
