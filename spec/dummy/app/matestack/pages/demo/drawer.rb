class Pages::Demo::Drawer < Matestack::Ui::Page

  def response
    components {

      heading size: 2, class: "mdc-typography--headline2", text: "Drawer"
      demo do
        material_drawer do
          material_drawer_nav do
            material_drawer_nav_item type: :link, path: "demo/drawer", icon: 'inbox', text: "Link"
            material_drawer_nav_item type: :transition, path: "/demo/drawer", icon: 'inbox', text: "Transition"
            material_drawer_nav_item type: :action, action_config: some_action_config, icon: 'inbox', text: "Action"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Sections"
      demo do
        material_drawer do
          material_drawer_nav do
            material_drawer_nav_item type: :link, path: "demo/drawer", icon: 'inbox', text: "Link"

            material_drawer_divider
            material_drawer_heading text: "My Section"

            material_drawer_nav_item type: :link, path: "demo/drawer", icon: 'folder', text: "Folder 1"
            material_drawer_nav_item type: :link, path: "demo/drawer", icon: 'folder', text: "Folder 2"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Drawer Header"
      demo do
        material_drawer do
          material_drawer_header do
            heading text: "Drawer with header"
            heading text: "including subtitle", size: 4
          end
          material_drawer_nav do
            material_drawer_nav_item type: :link, path: "/demo/drawer", icon: 'inbox', text: "Link"
          end
        end
        br
        plain "Drawers can contain a header element which will not scroll with the rest of the drawer content. Things like account switchers and titles should live in the header element."
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Modal Drawers"
      demo do
        material_top_app_bar fixed: true, text: "My App"
        material_drawer modal: true do
          material_drawer_nav do
            material_drawer_nav_item type: :action, action_config: some_action_config, icon: 'inbox', text: "Action"
          end
        end
        br
        br
        plain "Click on the top_app_bar menu icon in order to open or close the modal drawer."
      end

    }
  end

  def some_action_config
    {
      method: :get,
      path: '#',
      success: {
        emit: 'my_action_success'
      }
    }
  end

end
