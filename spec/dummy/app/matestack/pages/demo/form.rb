class Pages::Demo::Form < Matestack::Ui::Page

  def response
    components {

      heading size: 2, class: "mdc-typography--headline2", text: "Form"

      heading size: 3, class: "mdc-typography--headline3", text: "Text fields"
      demo do
        form some_config, :include do
          material_form_input key: :some_key, type: :text, material_type: :filled, label: "filled variant (default)"
          paragraph
          material_form_input key: :some_other_key, type: :text, material_type: :outlined, label: "outlined variant"
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Icons"
      demo do
        form some_config, :include do
          material_form_input key: :some_key, type: :text, label: "leading icon", icon: 'event'
          paragraph
          material_form_input key: :some_other_key, type: :text, label: "trailing icon", trailing_icon: 'delete'
          paragraph
          material_form_input key: :some_key, type: :text, material_type: :outlined, label: "leading icon", leading_icon: 'event'
          paragraph
          material_form_input key: :some_other_key, type: :text, material_type: :outlined, label: "trailing icon", trailing_icon: 'delete'
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Textarea"
      demo do
        form some_config, :include do
          material_form_input key: :some_key, type: :textarea, label: "textarea", rows: 8, cols: 40
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Other input types: number, email, date, password"
      demo do
        form some_config, :include do
          material_form_input key: :some_number_key, type: :number, label: "number"
          paragraph
          material_form_input key: :some_email_key, type: :email, label: "email"
          paragraph
          material_form_input key: :some_date_key, type: :date, label: "date"
          paragraph
          material_form_input key: :some_password_key, type: :password, label: "password"
        end
      end


      heading size: 3, class: "mdc-typography--headline3", text: "Select fields"
      demo do
        form some_config, :include do
          material_form_select key: :some_key, options: ["hello", "world"], type: :dropdown, material_type: :filled, label: "filled dropdown"
          paragraph
          material_form_select key: :some_key, options: ["hello", "world"], type: :dropdown, material_type: :outlined, label: "outlined dropdown"
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Option values and labels"
      demo do
        form some_config, :include do
          material_form_select key: :some_key, options: ["hello", "world"], type: :dropdown, label: "same values as labels"
          paragraph
          material_form_select key: :some_key, options: {"hello": 1, "world": 2}, type: :dropdown, label: "separate labels"
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "Manual initial value"
      demo do
        form some_config, :include do
          material_form_select key: :some_key, options: ["hello", "world"], type: :dropdown, label: "same values as labels", init: "world"
          paragraph
          material_form_select key: :some_key, options: {"hello": 1, "world": 2}, type: :dropdown, label: "separate labels", init: 2
          paragraph
          material_form_select key: :some_key, options: {"hello": 1, "world": 2}, type: :dropdown, label: "separate labels", init: "world"
        end
      end

      heading size: 4, class: "mdc-typography--headline4", text: "ActiveRecord Model Mapping"
      demo do
        @my_model = MyModel.new some_key: "world"
        form({for: @my_model, path: '#', method: 'post'}, :include) do
          material_form_select key: :some_key, options: ["hello", "world"], type: :dropdown, label: "init value from model"
          form_submit do
            material_button text: 'Submit me!'
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Checkboxes"
      demo do
        form some_config, :include do
          material_form_select key: :some_checkbox_key, options: ["hello", "world"], type: :checkbox, label: "checkboxes", init: ["hello"]
          paragraph
          form_submit do
            material_button text: 'Submit me!', type: :outlined
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Radio buttons"
      demo do
        form some_config, :include do
          material_form_select key: :some_radio_key, options: ["hello", "world"], type: :radio, label: "radio", init: "hello"
          paragraph
          form_submit do
            material_button text: 'Submit me!', type: :outlined
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Form with errors"
      demo do
        form form_with_error_config, :include do
          material_form_input key: :text_value_key, type: :text, label: "text value"
          paragraph
          material_form_select key: :dropdown_value_key, options: [nil, "hello", "world", "w", "t", "f"], label: "dropdown 1"
          paragraph
          material_form_input key: :text_value_key, type: :text, label: "text value"
          paragraph
          material_form_select key: :dropdown_2_value_key, material_type: :outlined, options: [nil, "hello", "world"], label: "dropdown 2"
          paragraph
          form_submit do
            material_button text: 'Submit me!', type: :outlined
          end
        end
      end

    }
  end

  def form_with_error_config
    {
      for: :my_object,
      method: :post,
      path: :demo_form_with_error_path,
      success: {
        emit: 'form_with_error_succeeded'
      },
      failure: {
        emit: 'form_with_error_failed',
        reset: false
      }
    }
  end

  def some_config
    {
      for: :my_object,
      method: :post,
      path: '#',
      params: {
        id: 42
      },
      success: {
        emit: 'my_action_success'
      }
    }
  end

end

class MyModel
  include ActiveModel::Model
  attr_accessor :some_key
end
