class Pages::Demo::Grid < Matestack::Ui::Page

  def response
    components {
      heading size: 2, class: "mdc-typography--headline2", text: "Grid"

      heading size: 3, class: "mdc-typography--headline3", text: "Basic Grid and Cells"
      demo do
        material_grid do
          material_grid_cell do
            plain "Cell 1"
          end
          material_grid_cell do
            plain "Cell 2"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Cell Spans"
      plain "Material design’s responsive UI is based on a column-variate grid layout. It has 12 columns on desktop, 8 columns on tablet and 4 columns on phone."
      demo do
        material_grid do
          material_grid_cell span: 3 do
            plain "Cell 1 with a span of 3"
          end
          material_grid_cell span: 9 do
            plain "Cell 2 with a span of 9"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Device-Specific Cell Spans"
      demo do
        material_grid do
          material_grid_cell phone: 8, tablet: 8, desktop: 6 do
            plain "Cell 1"
          end
          material_grid_cell phone: 4, tablet: 4, desktop: 6 do
            plain "Cell 2"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Nested Grids"
      demo do
        material_grid do
          material_grid_cell span: 6 do
            material_grid_nested do
              material_grid_cell span: 8 do
                plain "Cell 1.1"
              end
              material_grid_cell span: 4 do
                plain "Cell 1.2"
              end
            end
          end
          material_grid_cell span: 6 do
            plain "Cell 2"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Cell Order"
      plain "Optional, specifies the order of the cell. By default, items are positioned in the source order. However, you can reorder them by using the `order` parameter, which is an integer between 1 and 12."
      demo do
        material_grid do
          material_grid_cell order: 1 do
            plain "Cell 1, order 1"
          end
          material_grid_cell order: 3 do
            plain "Cell 2, order 3"
          end
          material_grid_cell order: 2 do
            plain "Cell 3, order 2"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Cell Alignment"
      plain "Optional, specifies the alignment of cell."
      demo do
        material_grid inner_style: "height: 100px" do
          material_grid_cell align: 'top' do
            plain "align: top"
          end
          material_grid_cell align: 'middle' do
            plain "align: middle"
          end
          material_grid_cell align: 'bottom' do
            plain "align: bottom"
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Grid Alignment"
      plain "The grid is by default center aligned. You can change this behavior by using the `align` parameter. Note, these modifiers will have no effect when the grid already fills its container."
      demo do
        material_grid align: 'left', style: "width: 50%" do
          material_grid_cell { plain "align: left" }
        end
        material_grid align: 'right', style: "width: 50%" do
          material_grid_cell { plain "align: right" }
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Grid With Fixed Column Width"
      plain "Optional, specifies the grid should have fixed column width."
      demo do
        material_grid fixed_column_width: true do
          material_grid_cell { plain "Cell 1" }
          material_grid_cell { plain "Cell 2" }
        end
      end

    }
  end

end