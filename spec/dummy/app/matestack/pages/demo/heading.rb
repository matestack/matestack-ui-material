class Pages::Demo::Heading < Matestack::Ui::Page

  def response
    components {
      material_heading size: 2, text: "Heading"

      demo do
        material_heading size: 3, text: "Some Heading"
        material_heading size: 3, class: "my-custom-class", id: "my-id" text: "Heading with custom class and id"
      end

    }
  end

end
