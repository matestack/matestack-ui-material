class Pages::Demo::Snackbar < Matestack::Ui::Page

  def response
    components {
      heading size: 2, class: "mdc-typography--headline2", text: "Snackbar"

      demo do
        material_snackbar show_on: 'some_event_1', hide_after: 5000 do
          material_snackbar_label text: "Yeah, 🍕, that's a fine snack!"
          material_snackbar_actions do
            onclick emit: "retry" do
              material_button text: "Retry"
            end
            material_snackbar_dismiss
          end
        end
        onclick emit: 'some_event_1' do
          material_button type: :outlined, text: "Show snackbar!"
        end
      end

      material_snackbar text: "Retry has worked! 😻", show_on: "retry", hide_after: 5000

      heading size: 3, class: "mdc-typography--headline3", text: "Minimal syntax"
      demo do
        material_snackbar text: "Yeah, 🍩, that's a fine snack!", show_on: 'some_event_2', hide_after: 5000
        onclick emit: 'some_event_2' do
          material_button type: :outlined, text: "Show snackbar!"
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Hide the snackbar manually"
      demo do
        material_snackbar text: "Yeah, 🍣, that's a fine snack!", show_on: 'some_event_3', hide_on: 'some_event_4'
        onclick emit: 'some_event_3' do
          material_button type: :outlined, text: "Show snackbar!"
        end
        onclick emit: 'some_event_4' do
          material_button text: "Hide snackbar"
        end
      end

    }
  end

end