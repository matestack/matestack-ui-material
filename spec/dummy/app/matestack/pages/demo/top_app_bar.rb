class Pages::Demo::TopAppBar < Matestack::Ui::Page

  def response
    components {
      heading size: 2, class: "mdc-typography--headline2", text: "TopAppBar"
      demo do
        material_top_app_bar do
          material_top_app_bar_left do
            material_top_app_bar_navigation_button icon: 'menu'
            material_top_app_bar_title text: "My App"
          end
          material_top_app_bar_right do
            action some_action_config do
              material_top_app_bar_action_item icon: 'bookmark'
            end
            transition path: '/demo/top_app_bar' do
              material_top_app_bar_action_item text: "Page 1"
            end
          end
        end
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Fixed Variant"
      demo do
        material_top_app_bar type: :fixed do
          material_top_app_bar_left do
            material_top_app_bar_navigation_button icon: 'menu'
            material_top_app_bar_title text: "My Fixed Top App Bar"
          end
        end

        br
        plain "Fixed top app bars stay at the top of the page and elevate above the content when scrolled."
      end

      heading size: 3, class: "mdc-typography--headline3", text: "Minimal Version"
      plain "To quickly add a bar with nav button and text, just provide the `text` parameter."
      demo do
        material_top_app_bar type: :fixed, text: "My Mini App Bar"
      end

    }
  end

  def some_action_config
    {
      method: :get,
      path: '#',
      success: {
        emit: 'my_action_success'
      }
    }
  end

end
