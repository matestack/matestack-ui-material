Rails.application.routes.draw do

  namespace :demo do
    get "/:component", to: 'pages#auto_resolve'
    scope :api do
      post "/form_with_error", to: 'api#form_with_error'
    end
  end

end
