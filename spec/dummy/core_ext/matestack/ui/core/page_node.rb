require 'matestack/ui/core/page_node'

class Matestack::Ui::Core::PageNode

  # Provide a demo method for our demo application.
  # https://gitlab.com/matestack/matestack-ui-material/issues/8#note_267351527
  #
  def demo(&block)
    material_grid class: 'demo-example' do
      material_grid_cell span: 6 do
        pre class: 'demo-code' do
          source = block.source.lines[1..-2].join
          source.gsub!(/^#{source.scan(/^[ \t]+(?=\S)/).min}/, '')
          plain source
        end
      end
      material_grid_cell span: 6 do
        div class: 'demo-stage' do
          self.instance_eval(&block)
        end
      end
    end
  end

end